
# Infrastructure
## Prerequisites
- Development
Java
Gradle

- Deployment
kind
kubectl
terraform
helm
## Environment Installation

# 1.Configure localhost
```
sudo vi /etc/hosts

## add those lines
127.0.0.1 keycloak.local
127.0.0.1 dashboard.apisix.local
127.0.0.1 admin.apisix.local
127.0.0.1 api.apisix.local
127.0.0.1 debezium-cluster-kafka-0.debezium-cluster-kafka-brokers.kafka-cdc.svc

```

# 2.Provision Kubernetes(k8s) cluster
```
cd k8s
kind create cluster --config ./kind-mscluster-config.yaml
```
# 2.1 Verify the cluster
```
kubectl cluster-info

# Output:
Kubernetes control plane is running at https://0.0.0.0:16443
CoreDNS is running at https://0.0.0.0:16443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```
# 3. Provision resources
## Via terraform: apisix, gitlab runner, keycloak, postgres
# 3.1 Execute terraform source code
```
# change values for gitlab docker registry on local.tfvar
# change configuration for gitlab runner
cd ..
cd terraform
terraform init
terraform plan --var-file=local.tfvars
terraform apply --var-file=local.tfvars
```
### Access [Keycloak Portal](http://keycloak.local/keycloak/)  admin/secretpassword
### Access [APISix Dashboard](http://dashboard.apisix.local/) - admin/admin

## Use yaml files: debezium, kafka
# 3.2 Apply yaml files
```
cd ../k8s/debezium
kubectl create ns kafka-cdc
kubectl config set-context --current --namespace=kafka-cdc
#Install strimzi through OLM
curl -sL https://github.com/operator-framework/operator-lifecycle-manager/releases/download/v0.20.0/install.sh | bash -s v0.20.0
kubectl create -f https://operatorhub.io/install/strimzi-kafka-operator.yaml

<<waiting atleast 10 mins>>

kubectl apply -f 1-kafka.yaml
kubectl apply -f 2-connector-secret.yaml
kubectl apply -f 3-debezium-secret-role.yaml
kubectl apply -f 4-debezium-secret-rolebinding.yaml
kubectl apply -f 5-debezium-registry-secret.yaml
kubectl apply -f 6-debezium-connector.yaml # ~ 10 min # ~ 10 min --> check status in [Custom Resource] --> kafka.strimzi.io --> KafkaConnect
kubectl apply -f 7-debezium-connector-postgres.yaml


#list all topic --> login to kafka & run. --- Using LENS is the best options
./bin/kafka-topics.sh --bootstrap-server=localhost:9092 --list
./bin/kafka-console-consumer.sh --bootstrap-server=kafka:9092 --topic postgres.public.product_event --from-beginning
```

## 4 Application Installation
#### Configure Keycloak
helm list -A

kubectl config set-context --current --namespace=default
# 4.1 Configuration
## Keycloak
- Import keycloak configuration: scripts/digital_realm.json
## Postman
- Import postman script & env: scripts/...; scripts/...
## Apisix
- Access Keycloak console to re-generate secret for *apisix_client*  
- Reconfig postman variable: backend_client_secret
##Job runner
disable public runner

## LRA and AMQ
```
cd ../../..
helm list -A
helm -n default upgrade --install lra-coordinator-service lra-coordinator/helm_lra/
helm -n default upgrade --install amq-broker-rhel8 lra-coordinator/helm_amq/
```
# 4.2 discovery-service
```
cd discovery-service
git checkout -b dev
gradle clean build -x test

git add .
git commit -m 'init'
git push --set-upstream origin dev
```
### Access [Discovery service]: http://api.apisix.local/eureka-service/

# 4.3 product-service
```
cd product-service
git checkout -b dev
gradle clean build -x test

git add .
git commit -m 'init'
git push --set-upstream origin dev
```
### Access [Product service]: http://api.apisix.local/product-service/swagger-ui/index.html

# 4.4 transaction-history-service
```
cd transaction-history-service
git checkout -b dev
gradle clean build -x test

git add .
git commit -m 'init'
git push --set-upstream origin dev
```
### Access [transaction history service]: http://api.apisix.local/transaction-history-service/swagger-ui/index.html

# 4.5 limit-service
```
cd limit-service
git checkout -b dev
gradle clean build -x test

git add .
git commit -m 'init'
git push --set-upstream origin dev
```

### Access [limit service]: http://api.apisix.local/limit-service/swagger-ui/index.html

# 4.6. internal-transfer-service
```
cd internal-transfer-service
git checkout -b dev
gradle clean build -x test

git add .
git commit -m 'init'
git push --set-upstream origin dev
```
### Access [internal transfer service]: http://api.apisix.local/internal-transfer-service/swagger-ui/index.html

# 4.7.  mock-integration-service
```
cd mock-integration-service
git checkout -b dev
gradle clean build -x test

git add .
git commit -m 'init'
git push --set-upstream origin dev
```
#### Access Service swagger page:
- Discovery service http://api.apisix.local/eureka-service/
- [APISix Dashboard](http://dashboard.apisix.local/) - admin/admin
- product-service http://api.apisix.local/product-service/swagger-ui/index.html
- internal-transfer-service - http://api.apisix.local/internal-transfer-service/swagger-ui/index.html
- limit-service - http://api.apisix.local/limit-service/swagger-ui/index.html

#### Postman
- Import postman script & env: scripts/...; scripts/...
- Access Keycloak console to re-generate secret for *apisix_client*
- Reconfig postman variable: backend_client_secret

## Demo Scenarios
##### Demo 1 - jwt token & authentication

- Run postman folder: *Demo 1: Access Token & Authentication*
- View ingress.yaml how ingress autheticate token.
- The flow: ![Alt text](jwt_token_validation_flow.png)

##### Demo 2 - API Gateway
APISix dashboard ![Alt text](apisix_dashboard.png)
- View ingress
- [APISix Dashboard](http://dashboard.apisix.local/) - admin/admin
  - dashboard.apisix.local
  - admin/admin
  - view plugins: openid-connect & rewrite

##### Demo 3 - Service Discovery (Eureka)
Eureka dashboard ![Alt text](discovery_with_eureka.png)
- View: http://api.apisix.local/eureka-service/
- View project: discovery service.

##### Demo 4 - Transactional messaging
Transactional messaging & CDC ![Alt text](cdc.png)
- CDC with Kafka & Debezium:
  - deployment script in: infrastructure/k8s
  - Run postman Folder 4.

##### Demo 5 - SAGA
Run Postman Folder 5
Refer pattern: https://jbossts.blogspot.com/2017/12/narayana-lra-implementation-of-saga.html?m=1
The flow: ![Alt text](saga.png)
##### Demo 6 - Observability
- View: http://api.apisix.local/limit-service/actuator
- View: http://api.apisix.local/product-service/actuator
##### Demo 7 - Deployment example
- View terraform & project (helm + Dockerfile).


##
```
helm -n default upgrade --install --set image.tag=8e19f1d1 limit-service limit-service/helm/
```
## Clean up

- Application clean up
```
`helm uninstall -n default transaction-history-service`
helm uninstall -n default product-service
helm uninstall -n default limit-service
helm uninstall -n default discovery-service
helm uninstall -n default lra-coordinator-service
helm uninstall -n default amq-broker-rhel8
helm uninstall -n default internal-transfer-service
helm uninstall -n default mock-integration-service
```

- Remove Gitlab Runer, Keycloak...
```
terraform destroy --var-file=local.tfvars
```
- Delete kind cluster
```
kind delete cluster --name=mscluster

#this will clean all clusters & data
kind delete clusters --all
```